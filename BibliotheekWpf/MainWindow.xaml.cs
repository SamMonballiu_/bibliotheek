﻿using BibliotheekLibrary;
using BibliotheekLibrary.Enums;
using BibliotheekLibrary.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BibliotheekWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        IUser _user;

        public MainWindow(IUser user)
        {
            InitializeComponent();
            _user = user;
            lblTest.Content = $"{user.Voornaam} {user.Familienaam}";
            //Task importCollection = new Task(() =>
            //{
            Debug.WriteLine("Started importing");
            if (CollectieBibliotheek.ItemsInCollectie.Count == 0)
            {
                CollectieManager.ImportCollectionFromCSV();
            }
            dgCollectie.ItemsSource = CollectieBibliotheek.ItemsInCollectie;
            //});

            IEnumerable<IUser> users = CollectieBibliotheek.Leden.Cast<IUser>().Union(CollectieBibliotheek.Medewerkers);
            lbUsers.ItemsSource = users;

            cbNewItemType.ItemsSource = Enum.GetNames(typeof(SoortItem));

            if (_user.GetType() == typeof(Lid))
            {
                spAdminPrivileges.Visibility = Visibility.Collapsed;
            }
        }

        private void btnLogOut_Click(object sender, RoutedEventArgs e)
        {
            Login login = new Login();
            login.Show();
            this.Close();
        }

        private void cbShowCheckedOutByUser_Checked(object sender, RoutedEventArgs e)
        {
            cbShowOnlyReservedByMe.IsChecked = false;
            var member = _user as IMember;
            dgCollectie.ItemsSource = member.ItemsUitgeleend;
            btnReserveItem.IsEnabled = false;
        }

        private void cbShowCheckedOutByUser_Unchecked(object sender, RoutedEventArgs e)
        {
            dgCollectie.ItemsSource = CollectieBibliotheek.ItemsInCollectie;
            btnReserveItem.IsEnabled = true;
        }

        private void txtSearchTerm_TextChanged(object sender, TextChangedEventArgs e)
        {
            var senderTb = sender as TextBox;
            var searchText = senderTb.Text.ToUpper();

            if (senderTb.Text == String.Empty)
            {
                dgCollectie.ItemsSource = CollectieBibliotheek.ItemsInCollectie;
                return;
            }

            dgCollectie.ItemsSource = CollectieBibliotheek.ItemsInCollectie.Where(x =>
                x.Auteur.ToUpper().Contains(searchText) ||
                x.Titel.ToUpper().Contains(searchText) ||
                x.Jaartal.ToString().Contains(searchText) ||
                x.Reservatienaam.ToUpper().Contains(searchText)
                );
        }


        private void btnCheckoutItem_Click(object sender, RoutedEventArgs e)
        {
            if (dgCollectie.SelectedItem is null) { return; }

            var selected = dgCollectie.SelectedItem as Item;
            var result = (_user as IMember).Uitlenen(CollectieBibliotheek.ItemsInCollectie.Where(x => x.ItemId == selected.ItemId).FirstOrDefault());
            MessageBox.Show(result ? "Item checked out" : "Could not check item out.");
            dgCollectie.Items.Refresh();
        }

        private void btnReturnItem_Click(object sender, RoutedEventArgs e)
        {
            if (dgCollectie.SelectedItem is null) { return; }

            var selected = dgCollectie.SelectedItem as Item;

            var member = _user as IMember;
            if (member.ItemsUitgeleend.Contains(selected))
            {
                member.Terugbrengen(CollectieBibliotheek.ItemsInCollectie.Where(x => x.ItemId == selected.ItemId).FirstOrDefault());
                MessageBox.Show("Item returned");
                member.ItemsUitgeleend.Remove(selected);
            }

            else
            {
                MessageBox.Show("That item is not currently checked out by\n" + _user.ToString());
            }
            dgCollectie.Items.Refresh();
        }

        private void btnReserveItem_Click(object sender, RoutedEventArgs e)
        {
            if (dgCollectie.SelectedItem is null) { return; }
            var selected = dgCollectie.SelectedItem as Item;
            var member = _user as IMember;

            if (member.Reserveren(selected))
            {
                MessageBox.Show("Item reserved.");
                dgCollectie.Items.Refresh();
            }

            else
            {
                MessageBox.Show("Could not reserve item.");
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            CollectieManager.SaveCollection(CollectieBibliotheek.ItemsInCollectie);
            CollectieManager.ExportAdminsAsCSV();
            CollectieManager.ExportMembersAsCSV();
            CollectieManager.ExportCollectionAsCSV();
        }

        private void lbUsers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DetermineUserAction();

        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            var senderRb = sender as RadioButton;
            DetermineUserAction(senderRb);
        }

        private void DetermineUserAction(RadioButton senderRb = null)
        {
            var user = lbUsers.SelectedItem as IMember;

            if (senderRb is null)
            {
                List<RadioButton> rButtons = new List<RadioButton>()
                {
                    rbHistory, currentItems, reservedItems
                };

                senderRb = rButtons.FirstOrDefault(x => (bool)x.IsChecked);
            }

            switch (senderRb?.Name)
            {
                case "rbHistory":
                    dgAdmin.ItemsSource = user?.Uitleenhistoriek.Select(x => x.Item);
                    break;
                case "currentItems":
                    dgAdmin.ItemsSource = user?.ItemsUitgeleend;
                    break;
                case "reservedItems":
                    dgAdmin.ItemsSource = CollectieBibliotheek.ItemsInCollectie.Where(x => x.Reservatienaam == $"{user.Voornaam} {user.Familienaam}");
                    break;
            }
        }

        private void btnAddNewItem_Click(object sender, RoutedEventArgs e)
        {
            if (NewItemInputValidation())
            {
                tbValidateResult.Text = String.Empty;
                var soortItem = (SoortItem)cbNewItemType.SelectedIndex;
                var newItem = Factory.CreateItem(soortItem, txtTitle.Text, txtAuthor.Text, int.Parse(txtYear.Text));


                if (CollectieBibliotheek.ItemsInCollectie.Contains(newItem))
                {
                    MessageBox.Show("An item with these characteristics is already in the collection.");
                    return;
                }

                CollectieBibliotheek.ItemsInCollectie.Add(newItem);
                txtAuthor.Text = txtTitle.Text = txtYear.Text = String.Empty;
                cbNewItemType.SelectedIndex = -1;
                dgCollectie.Items.Refresh();
                dgCollectie.ScrollIntoView(newItem);
            }
        }

        private bool NewItemInputValidation()
        {
            string errors = String.Empty;

            List<TextBox> textBoxes = new List<TextBox>()
            {
                txtAuthor,
                txtTitle,
                txtYear
            };

            if (textBoxes.Any(x => x.Text == String.Empty))
            {
                errors += "All fields must be filled out. ";
            }

            if (txtYear.Text.Any(x => !Char.IsDigit(x)) || txtYear.Text.Length < 4 || !int.TryParse(txtYear.Text, out int _))
            {
                errors += "Invalid year. ";
            }

            if (cbNewItemType.SelectedIndex == -1)
            {
                errors += "No type selected. ";
            }


            if (errors == String.Empty)
            {
                return true;
            }

            else
            {
                tbValidateResult.Text = errors;
                return false;
            }
        }

        private void miClearReserved_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Are you sure?", "Clear reserved status", MessageBoxButton.YesNo) == MessageBoxResult.No)
            {
                return;
            }

            var contextMenu = (sender as MenuItem).Parent as ContextMenu;
            var dataGrid = (DataGrid)contextMenu.PlacementTarget;

            var item = dgAdmin.SelectedItem as Item;
            item.Reservatienaam = "";
            item.Gereserveerd = false;

        }

        private void cbShowOnlyReservedByMe_Checked(object sender, RoutedEventArgs e)
        {
            cbShowCheckedOutByUser.IsChecked = false;
            dgCollectie.ItemsSource = CollectieBibliotheek.ItemsInCollectie.Where(x => x.Reservatienaam == $"{_user.Voornaam} {_user.Familienaam}");
            btnReserveItem.IsEnabled = false;
        }

        private void cbShowOnlyReservedByMe_Unchecked(object sender, RoutedEventArgs e)
        {
            dgCollectie.ItemsSource = CollectieBibliotheek.ItemsInCollectie;
            btnReserveItem.IsEnabled = true;
        }
    }
}
