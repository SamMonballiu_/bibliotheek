﻿using BibliotheekLibrary;
using BibliotheekLibrary.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;

namespace BibliotheekWpf
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        public Login()
        {
            InitializeComponent();

            if (CollectieBibliotheek.Medewerkers.Count == 0)
            {
                CollectieManager.ImportAdminsFromCSV();
            }

            if (CollectieBibliotheek.Leden.Count == 0)
            {
                CollectieManager.ImportMembersFromCSV();
            }

            Debug.WriteLine(string.Join("\n", CollectieBibliotheek.Leden));
            Debug.WriteLine(string.Join("\n", CollectieBibliotheek.Medewerkers));

            List<IUser> allUsers = new List<IUser>();

            foreach (var item in CollectieBibliotheek.Medewerkers)
            {
                allUsers.Add(item);
            }

            foreach (var item in CollectieBibliotheek.Leden)
            {
                allUsers.Add(item);
            }

            lbUsers.ItemsSource = allUsers;
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                IUser sel = lbUsers.SelectedItem as IUser;
                MainWindow main = new MainWindow(sel);
                main.Show();
                this.Close();
            }
            catch (System.Exception err)
            {
                MessageBox.Show(err.ToString());
            }
        }

        private void btnNewMember_Click(object sender, RoutedEventArgs e)
        {
            if (txtLastName.Text == string.Empty || txtFirstName.Text == string.Empty)
            {
                return;
            }

            Lid newMember = Factory.CreateLid(txtLastName.Text, txtFirstName.Text, DateTime.Now);
            CollectieBibliotheek.Leden.Add(newMember);
            MainWindow main = new MainWindow(newMember);
            main.Show();
            this.Close();
        }
    }
}
